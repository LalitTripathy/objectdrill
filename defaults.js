function defaults(obj, defaultProps) {
    
    for (let key in obj){
        if (!obj[key]){
            obj[key] = defaultProps[key];
        }
    }
    delete obj.myDefault;
    return obj

}
module.exports = {defaults};