let testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const key = require('../mapObject');
let cb = a => a.toString();

let answer = key.mapObject(testObject,cb);
// testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

if (JSON.stringify(answer) === JSON.stringify(testObject)) {
    console.log(answer);
}
else {
    console.log("wrong output");
}
